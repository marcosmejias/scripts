#!/bin/bash

# Exit if no root privileges
if [[ $UID -ne 0 ]]; then
  >&2 echo "Root privileges needed to run... exiting"
  exit 1  
fi

# Exit if recovery destination not mounted
recovery_mount=/mnt/recovery
mountpoint -q $recovery_mount
if [[ $? -ne 0 ]]; then
  >&2 echo "Recovery partition is not mounted... exiting"
  exit 2
fi 

# Back up with date YYYY MMM DD
date=$(date +%Y-%b-%d)
backup_prefix="claire-root.backup"
backup_dir="$backup_prefix.$date"
backup_filename="$backup_dir.tar.zst"
backup_path="$backup_dir/$backup_filename"

[[ ! -d "$backup_dir " ]] && mkdir $backup_dir

echo "Archiving root partition..."
tar cvpf "$backup_path" --use-compress-program=zstd --exclude-from=backup-exclude.txt /

if [[ $? -eq 0 ]]; then
  echo "Calculating archive's checksums..."
  cd $backup_dir
  sha512sum $backup_filename > $backup_filename.sha512sum
  md5sum $backup_filename > $backup_filename.md5sum
  cd ../
  echo "Moving archive to recovery partition..."
  mv $backup_dir $recovery_mount
else
  >&2 echo "Compression has failed... cleaning up and exiting"
  rm -r ./$backup_dir
  exit 3
fi
